(function ($) {
  Drupal.behaviors.multicurrency = {
    attach: function (context, settings) {
      $('#block-multicurrency-changer', context).once('multicurrency-changer', function () {
        $(this).find('.currency-list li').click(function (event) {
          var $item = $(this);
          $.cookie('Drupal.visitor.currency_code', $item.data('currencyCode'), {
            path: Drupal.settings.basePath,
            expires: 365
          });
          location.reload();
        });
      });
    }
  };
})(jQuery);
