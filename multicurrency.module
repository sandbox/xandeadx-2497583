<?php

/**
 * Implements hook_cron().
 */
function multicurrency_cron() {
  $result = file_get_contents('http://www.nbrb.by/Services/XmlExRates.aspx?ondate=' . date('m/d/Y'));
  $xml = simplexml_load_string($result);
  $currencies = multicurrency_get_currencies();

  foreach ($xml->Currency as $currency) {
    $currency_code = (string)$currency->CharCode;
    if (isset($currencies[$currency_code])) {
      $currencies[$currency_code]->field_currency_rate['und'][0]['value'] = (string)$currency->Rate;
      taxonomy_term_save($currencies[$currency_code]);
    }
  }

  cache_clear_all('field:taxonomy_term:', 'cache_field', TRUE);
  watchdog('multicurrency', 'Курсы валют обновлены', NULL);
}

/**
 * Return currecy by code.
 */
function multicurrency_get_currency($currency_code) {
  $result = EntityHelper::getEntityByConditions('taxonomy_term', NULL, array(
    'field_currency_code' => array(
      'value' => $currency_code,
    ),
  ));
  return taxonomy_term_load($result->tid);
}

/**
 * Return currency list.
 */
function multicurrency_get_currencies() {
  $currencies_tids = array_keys(EntityHelper::getEntitiesByConditions('taxonomy_term', array('vid' => 3)));
  $currencies = array();
  foreach (taxonomy_term_load_multiple($currencies_tids) as $currency) {
    $currency_code = $currency->field_currency_code['und'][0]['value'];
    $currencies[$currency_code] = $currency;
  }
  return $currencies;
}

/**
 * Return current currency code.
 */
function multicurrency_get_current_cyrrency_code() {
  if (!empty($_COOKIE['Drupal_visitor_currency_code'])) {
    return $_COOKIE['Drupal_visitor_currency_code'];
  }
  else {
    return $GLOBALS['language']->language == 'ru' ? 'BYR' : 'EUR';
  }
}

/**
 * Convert one currency to another.
 */
function multicurrency_convert($price, $currency_code = NULL) {
  if ($price == 0) {
    return 0;
  }
  if (!$currency_code) {
    $currency_code = multicurrency_get_current_cyrrency_code();
  }
  $currency = multicurrency_get_currency($currency_code);
  $currency_rate = $currency->field_currency_rate['und'][0]['value'];
  return ceil($price / $currency_rate);
}

/**
 * Implements hook_block_info().
 */
function multicurrency_block_info() {
  $blocks['changer'] = array(
    'info' => 'Выбор валюты',
  );
  return $blocks;
}
 
/**
 * Implements hook_block_view().
 */
function multicurrency_block_view($delta = '') {
  $block = array();
 
  if ($delta == 'changer') {
    $items = array();
    $current_currency_code = multicurrency_get_current_cyrrency_code();
    
    foreach (multicurrency_get_currencies() as $currency) {
      $currency_code = $currency->field_currency_code['und'][0]['value'];
      $item = array(
        'data' => '
          <span class="currency-symbol">' . $currency->field_currency_symbol['und'][0]['value'] . '</span>
          <span class="currency-name">' . $currency->name . '</span>
        ',
        'data-currency-code' => $currency_code,
      );
      if ($currency_code == $current_currency_code) {
        $item['class'][] = 'active';
      }
      $items[] = $item;
    }
    
    $block['content']['#theme'] = 'item_list';
    $block['content']['#items'] = $items;
    $block['content']['#attributes']['class'][] = 'currency-list';
    $block['content']['#attached']['library'][] = array('system', 'jquery.cookie');
    $block['content']['#attached']['js'][] = drupal_get_path('module', 'multicurrency') . '/multicurrency.js';
  }
 
  return $block;
}

/**
 * Preprocess vars for page.tpl.php.
 */
function multicurrency_preprocess_html(&$vars) {
  $vars['classes_array'][] = 'currency-' . drupal_strtolower(multicurrency_get_current_cyrrency_code());
}
